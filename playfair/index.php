<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 09.11.17
 * Time: 20:08
 */

ini_set("display_errors", "on");

include_once __DIR__."/playfair.php";

$playfair = new PlayFairCypher();
$text = "Wikipedie, otevřená encyklopedie";
$encrypted = $playfair->Encrypt($text, "PLAYFAIROVA SIFRA2");
echo "Text: $text <br>";
echo "Encrypted: $encrypted <br>";
$decrypted = $playfair->Decrypt($encrypted, "PLAYFAIROVA SIFRA2");
echo "Decrypted: $decrypted <br>";


