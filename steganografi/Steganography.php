<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 08.10.17
 * Time: 19:25
 */
ini_set("display_errors", "on");

class Steganography
{

    private $outputFolder = "encrypted/";
    private $image_src = null;

    function __construct($image_src = null)
    {
        $this->image_src = $image_src;
    }

    function Encode($msq, $src = null)
    {
        if ($src == null) {
            $src = $this->image_src;
        }

        $binary_message = $this->toBin($msq);
        $message_length = strlen($binary_message);
        $type = exif_imagetype($src);
        if ($type == 1) {
            $im = imagecreatefromgif($src);
        } else if ($type == 2) {
            $im = imagecreatefromjpeg($src);
        } else if ($type == 3) {
            $im = imagecreatefrompng($src);
        } else {
            die((object)array("bits_length" => "Nepodporovaný formát!!"));
        }
        //$im = imagecreatefromjpeg($src);
        $i = 0;
        for ($y = 0; $y < imagesy($im); $y++) {
            for ($x = 0; $x < imagesx($im); $x++) {
                $rgb = imagecolorat($im, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                if ($i >= $message_length || !isset($binary_message[$i])) {
                    $newR = $r;
                    $i++;
                } else {
                    $newR = $this->toBin($r);
                    $newR[strlen($newR) - 1] = $binary_message[$i];
                    $newR = $this->bin2text($newR);
                    $i++;
                }

                if ($i >= $message_length || !isset($binary_message[$i])) {
                    $newG = $g;
                    $i++;
                } else {
                    $newG = $this->toBin($g);
                    $newG[strlen($newG) - 1] = $binary_message[$i];
                    $newG = $this->bin2text($newG);
                    $i++;
                }

                if ($i >= $message_length || !isset($binary_message[$i])) {
                    $newB = $b;
                } else {
                    $newB = $this->toBin($b);
                    $newB[strlen($newB) - 1] = $binary_message[$i];
                    $newB = $this->bin2text($newB);
                    $i++;
                }
                $new_color = imagecolorallocate($im, $newR, $newG, $newB);
                imagesetpixel($im, $x, $y, $new_color);
            }
        }
        $file = basename($src);         // uploads/file.jpeg => "file.jpeg"
        $file = basename($file, ".jpg");
        $file = basename($file, ".jpeg");
        $file = basename($file, ".png");
        $file = basename($file, ".gif");
        if ($type == 1) {
            //imagegif($im, $this->outputFolder . 'simple.gif'); // pro gif nefunguje decryp, odzkoušet
            imagepng($im, $this->outputFolder . $file . '.png');
        } else if ($type == 2) {
            imagepng($im, $this->outputFolder . $file . '.png');
            //imagejpeg($im, $this->outputFolder . 'simple.jpg'); // pro jpg nefunguje decrypt
        } else if ($type == 3) {
            imagepng($im, $this->outputFolder . $file . '.png');
        } else {
            die ("chyba");
        }
        imagedestroy($im);
        return (object)array("bits_length" => $message_length);
    }

    function Decode($bits)
    {
        $src = $this->outputFolder . $this->image_src;
        $type = exif_imagetype($src);
        if ($type == 1) {
            $im = imagecreatefromgif($src);
        } else if ($type == 2) {
            $im = imagecreatefromjpeg($src);
        } else if ($type == 3) {
            $im = imagecreatefrompng($src);
        } else {
            die((object)array("bits_length" => "Nepodporovaný formát!!"));
        }
        $real_message = null;
        $i = 0;
        $maxBits = $bits;
        for ($y = 0; $y < imagesy($im); $y++) {
            for ($x = 0; $x < imagesx($im); $x++) {
                if ($i == $maxBits) {
                    break;
                }
                $rgb = imagecolorat($im, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $red = $this->toBin($r);
                $real_message .= $red[strlen($red) - 1];
                $i++;
                if ($i == $maxBits) {
                    break;
                }
                $g = ($rgb >> 8) & 0xFF;
                $green = $this->toBin($g);
                $real_message .= $green[strlen($green) - 1];
                $i++;
                if ($i == $maxBits) {
                    break;
                }
                $b = $rgb & 0xFF;
                $blue = $this->toBin($b);
                $real_message .= $blue[strlen($blue) - 1];
                $i++;
            }
        }
        $real_message = $this->bin2text($real_message, false);
        return $real_message;
    }

    private function toBin($str)
    {
        $str = (string)$str;
        $l = strlen($str);
        $result = '';
        while ($l--) {
            $result = str_pad(decbin(ord($str[$l])), 8, "0", STR_PAD_LEFT) . $result;
        }
        return $result;
    }

    private function bin2text($bin, $int = true)
    {
        $text = null;

        $chars = explode("\n", chunk_split(str_replace("\n", '', $bin), 8));
        for ($i = 0; $i < count($chars); $i++) {
            $text .= chr(bindec($chars[$i]));
        }
        # let's return the result
        if ($int) {
            return (int)$text;
        } else {
            return (string)$text;
        }
    }

    function listDecryptedImages()
    {
        $dir = $this->outputFolder;
        $result = array();
        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                $result[] = $value;
            }
        }
        return $result;
    }
}