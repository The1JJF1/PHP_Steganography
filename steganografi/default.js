$(function () {
    var files;

    $("#form1").on("submit", function (e) {
        e.preventDefault();
        var text = $("#textToEncrypt").val();
        $("#submit").prop("disabled", true);

        $("#message").text("Nahrávání..");

        // Create a formdata object and add the files
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        $.ajax({
            url: 'fcs.php?files',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    // spustí encrypt
                    $.post("fcs.php?encrypt=true&src=" + data.files[0] + "&msg=" + text + "&uploads=false", {}, function (clbck) {
                        //console.log(clbck);
                        $("#message").html("Dokončeno<br>" +
                            "Bits Length: " + clbck.bits_length
                        );
                        $("#submit").prop("disabled", false);
                    }, "json");
                }
                else {
                    // Handle errors here
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });

    });

    $("#file").on("change", function (e) {
        files = e.target.files;

        if ($(this).val() !== "" && $("#textToEncrypt").val().length > 2) {
            $("#submit").prop("disabled", false);
        } else {
            $("#submit").prop("disabled", true);
        }
    });
    $("#textToEncrypt").on('change keyup paste', function () {
        if ($(this).val().length > 2 && $("#file").val() !== "") {
            $("#submit").prop("disabled", false);
        } else {
            $("#submit").prop("disabled", true);
        }
    });

    /**
     * Zjištění infa před uploadem
     */
    window.URL = window.URL || window.webkitURL;
    var elBrowse = document.getElementById("file"),
        elPreview = document.getElementById("preview"),
        useBlob = false && window.URL; // Set to `true` to use Blob instead of Data-URL

// 2.
    function readImage(file) {

        // Create a new FileReader instance
        // https://developer.mozilla.org/en/docs/Web/API/FileReader
        var reader = new FileReader();

        // Once a file is successfully readed:
        reader.addEventListener("load", function () {

            // At this point `reader.result` contains already the Base64 Data-URL
            // and we've could immediately show an image using
            // `elPreview.insertAdjacentHTML("beforeend", "<img src='"+ reader.result +"'>");`
            // But we want to get that image's width and height px values!
            // Since the File Object does not hold the size of an image
            // we need to create a new image and assign it's src, so when
            // the image is loaded we can calculate it's width and height:
            var image = new Image();
            image.addEventListener("load", function () {
                var $infoTable = $("table#infoFile");
                $infoTable.find("tr#noSelectedImage").remove();
                console.log(image);
                console.log(this);
                console.log($(this));
                $infoTable.append("<tr id='noSelectedImage'>" +
                    "<td><img src='" + image.src + "'></td>" +
                    "<td>" + file.name + "</td>" +
                    "<td>" + Math.round(file.size / 1024) + 'KB' + "</td>" +
                    "<td>" + image.width + '×' + image.height + "</td>" +
                    "<td>" + file.type + "</td>" +
                    "<td>" + Math.round((image.width * image.height) * 3 / 8) + "</td>" +
                    "</tr>");
                // Finally append our created image and the HTML info string to our `#preview`
                //elPreview.appendChild(this);
                //elPreview.insertAdjacentHTML("beforeend", imageInfo + '<br>');

                // If we set the variable `useBlob` to true:
                // (Data-URLs can end up being really large
                // `src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAA...........etc`
                // Blobs are usually faster and the image src will hold a shorter blob name
                // src="blob:http%3A//example.com/2a303acf-c34c-4d0a-85d4-2136eef7d723"
                if (useBlob) {
                    // Free some memory for optimal performance
                    window.URL.revokeObjectURL(image.src);
                }
            });

            image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;

        });

        // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
        reader.readAsDataURL(file);
    }

// 1.
// Once the user selects all the files to upload
// that will trigger a `change` event on the `#browse` input
    elBrowse.addEventListener("change", function () {

        // Let's store the FileList Array into a variable:
        // https://developer.mozilla.org/en-US/docs/Web/API/FileList
        var files = this.files;
        // Let's create an empty `errors` String to collect eventual errors into:
        var errors = "";

        if (!files) {
            errors += "File upload not supported by your browser.";
        }

        // Check for `files` (FileList) support and if contains at least one file:
        if (files && files[0]) {

            // Iterate over every File object in the FileList array
            for (var i = 0; i < files.length; i++) {

                // Let's refer to the current File as a `file` variable
                // https://developer.mozilla.org/en-US/docs/Web/API/File
                var file = files[i];

                // Test the `file.name` for a valid image extension:
                // (pipe `|` delimit more image extensions)
                // The regex can also be expressed like: /\.(png|jpe?g|gif)$/i
                if ((/\.(png|jpeg|jpg|gif)$/i).test(file.name)) {
                    // SUCCESS! It's an image!
                    // Send our image `file` to our `readImage` function!
                    readImage(file);
                } else {
                    errors += file.name + " Unsupported Image extension\n";
                }
            }
        }

        // Notify the user for any errors (i.e: try uploading a .txt file)
        if (errors) {
            alert(errors);
        }

    });
});