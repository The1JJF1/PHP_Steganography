<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 06.10.17
 * Time: 10:36
 */
ini_set("display_errors", "on");
?>
<html>
<head>
    <title>Steganografie</title>
    <meta charset="UTF-8">
    <link href="default.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-latest.min.js"
            type="text/javascript"></script>
    <script src="default.js"></script>
</head>
<body>
<div class="container">
    <h1>Steganografie - Encode</h1>
    <a href="decode.php">Decode</a>

    <form id="form1">
        <label>
            <span>Zvolte obrázek</span>
            <input type="file" id="file">
        </label>
        <label>
            <span>Zadejte text, který chcete zašifrovat</span>
            <textarea id="textToEncrypt" placeholder="Váš text"></textarea>
        </label>
        <label>
            <input type="submit" value="Vytvořit" disabled id="submit">
        </label>
    </form>
    <div id="message"></div>
    <hr>
    <table id="infoFile">
        <tr>
            <th></th>
            <th>Název</th>
            <th>Velikost souboru</th>
            <th>Rozlišení</th>
            <th>Formát</th>
            <th>Max. znaků</th>
        </tr>
        <tr id="noSelectedImage">
            <td colspan="6">Žádný vybraný soubor</td>
        </tr>
    </table>
</div>

</body>
</html>

