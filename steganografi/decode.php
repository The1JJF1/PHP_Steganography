<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 09.10.17
 * Time: 9:14
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 06.10.17
 * Time: 10:36
 */
ini_set("display_errors", "on");
?>
<html>
<head>
    <title>Steganografie</title>
    <meta charset="UTF-8">
    <link href="default.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-latest.min.js"
            type="text/javascript"></script>
    <script src="decrypt.js"></script>
</head>
<body>
<div class="container">
    <h1>Steganografie - Decode</h1>
    <a href="index.php">Encode</a>
    <form id="form2">
        <label>
            <span>Výběr encryptovaného obrázku</span><br>
            <select id="fileToDecrypt">
                <option value="">- Vyberte obrázek -</option>
                <?php
                include_once __DIR__ . "/Steganography.php";
                $steganography = new Steganography();
                foreach ($steganography->listDecryptedImages() as $index => $data) {
                    echo "<option value='$data'>$data</option>";
                }
                ?>
            </select>
        </label>
        <label>
            <span>Zadejte Bits Length</span>
            <input type="number" id="bitLengths">
        </label>
        <label>
            <input type="submit" value="Decryptovat" disabled id="submitDecrypt">
        </label>
        <hr>
        <label>
            <span>Dešifrovaný text z obrázku</span>
            <textarea id="textDecrypted" disabled></textarea>
        </label>
    </form>
</div>

</body>
</html>


